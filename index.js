// let a, b;

// function getMax(a, b) {
//    return (a > b) ? b : a;
// }

// let number = getMax(43, 12);
// console.log("Nilai terbesar adalah", number);

// let height, width;
// function isLandscape(height, width) {
//    return (height < width);
// }
// console.log(isLandscape(100, 400))

let input;

const games = FizzBuzz(false);
console.log(games);

function FizzBuzz(input) {
  if (typeof input !== "number") {
    return "Is Not A Number";
  }
  if (input % 3 === 0 && input % 5 === 0) {
    return "FizzBuzz";
  }

  if (input % 3 === 0) {
    return "Fizz";
  }
  if (input % 5 === 0) {
    return "Buzz";
  }
  return input;
}
