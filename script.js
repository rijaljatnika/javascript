const express = require("express");
const app = express();
const Joi = require('joi');

app.use(express.json());

const courses = [{
        id: 1,
        name: 'Rijal'
    },
    {
        id: 2,
        name: 'Winda'
    },
    {
        id: 3,
        name: 'Rahmat'
    },
];

app.get('/', (req, res) => {
    res.send('hello world');
});

app.get('/api/courses/', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    let person = courses.find(nama => nama.id === parseInt(req.params.id));
    if (!person) {
        res.status(404).send('Nama yang anda cari tidak ditemukan!');
    }
    res.send(person);
});

app.post('/api/courses/', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    };
    const result = Joi.validate(req.body, schema);
    console.log(result);

    if (!req.body.name || req.body.name.length < 3) {
        // 400 bad status
        res.status(404).send('harus lebih dari 3 character');
        return;
    }

    const user = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(user);
    res.send(user);

    // app.post('/admin/', (req, res) => {
    //     const schema = {
    //         name: Joi.string().min(3).required()
    //     };

    //     const result = Joi.validate(req.body, schema);
    //     console.log(result);

    //     if (!req.body.name || req.body.name.length < 3) {
    //         // 400 bad status
    //         res.status(404).send('harus lebih dari 3 character');
    //         return;
    //     }

    //     const user = {
    //         id: users.length + 1,
    //         name: req.body.name,
    //         umur: req.body.umur
    //     };
    //     users.push(user);
    //     res.send(user)
    // })


});

let port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on Port ${port} ...`));

// const express = require('express');
// const Joi = require('joi');
// let app = express();

// app.use(express.json());

// let users = [{
//         id: 1,
//         name: 'Rijal',
//         umur: 20
//     },
//     {
//         id: 2,
//         name: 'Adam',
//         umur: 24
//     }, {
//         id: 3,
//         name: 'Winda',
//         umur: 21
//     }, {
//         id: 4,
//         name: 'Maman',
//         umur: 42
//     }, {
//         id: 5,
//         name: 'Ramzi',
//         umur: 25
//     }
// ]

// app.get('/', (req, res) => {
//     res.send('hello ');
// });

// app.get('/admin/', (req, res) => {
//     res.send(users);
// });

// app.get('/admin/:id', (req, res) => {
//     const person = users.find(user => user.id === parseInt(req.params.id));
//     if (!person) send.status(404).send('Anda Bukan Admin');
//     res.send(person);
// });

// app.post('/admin/', (req, res) => {
//     const schema = {
//         name: Joi.string().min(3).required()
//     };

//     const result = Joi.validate(req.body, schema);
//     console.log(result);

//     if (!req.body.name || req.body.name.length < 3) {
//         // 400 bad status
//         res.status(404).send('harus lebih dari 3 character');
//         return;
//     }

//     const user = {
//         id: users.length + 1,
//         name: req.body.name,
//         umur: req.body.umur
//     };
//     users.push(user);
//     res.send(user)
// })


// let port = process.env.PORT || 3333;
// app.listen(port, () => console.log(`listening on port ${port}`));